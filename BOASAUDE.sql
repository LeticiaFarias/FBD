Create Table Paciente (
	codpac INTEGER,
	nome VARCHAR(80),
	endereco VARCHAR(100),
	telefone VARCHAR(12),
	CONSTRAINT Paciente_pkey PRIMARY KEY (codpac)
);

CREATE TABLE Medico (
	CRM INTEGER,
	nome VARCHAR(80),
	endereco VARCHAR(100),
	telefone VARCHAR(12),
	especialidade VARCHAR(50),
	CONSTRAINT Medico_pkey PRIMARY KEY (CRM)
);

CREATE TABLE Convenio (
	Codconv INTEGER,
	nome VARCHAR(100),
	CONSTRAINT Convenio_pkey PRIMARY KEY (Codconv)
);

CREATE TABLE Consulta (
	Codconsulta INTEGER,
	data DATE,
	horario TIME,
	medico INTEGER,
	paciente INTEGER,
	convenio INTEGER,
	porcent INTEGER,
	CONSTRAINT codconsulta_pkey PRIMARY KEY (Codconsulta),
	CONSTRAINT medico_fkey FOREIGN KEY (medico)
	REFERENCES Medico (CRM), 
	CONSTRAINT paciente_fkey FOREIGN KEY (paciente)
	REFERENCES Paciente(codpac),
	CONSTRAINT convenio_fkey FOREIGN KEY (convenio)
	REFERENCES Convenio(Codconv)
);

CREATE TABLE Atende (
	Medico INTEGER,
	convenio INTEGER,
	CONSTRAINT cedico_fkey FOREIGN KEY (medico)
	REFERENCES Medico(CRM),
	CONSTRAINT convenio_fkey FOREIGN KEY (convenio)
	REFERENCES Convenio(codconv)
);

CREATE TABLE Possui (
	Paciente INTEGER,
	convenio INTEGER,
	tipo VARCHAR(80),
	vencimento DATE,
	CONSTRAINT Paciente_fkey FOREIGN KEY (Paciente)
	REFERENCES Paciente (codpac),
	CONSTRAINT convenio_fkey FOREIGN KEY (convenio)
	REFERENCES Convenio (codconv)
);

INSERT INTO Paciente VALUES(1, 'João', 'Rua 1', '9809-9756'),
(2, 'José', 'Rua B', '3621-8978'),
(3, 'Maria', 'Rua 10', '4567-9872'),
(4, 'Joana', 'Rua J', '3343-9889');

SELECT * FROM PACIENTE

INSERT INTO Medico VALUES (18739, 'Elias', 'Rua X', '8738-1221', 'Pediatria'),
(7646, 'Ana', 'Av Z', '7829-1233', 'Obstetricia'),
(39872, 'Pedro', 'Tv H', '9888-2333', 'Oftalmologia');

SELECT * FROM Medico;

INSERT INTO Convenio VALUES (189, 'Cassi'),
(232, 'Unimed'),
(454, 'Santa Casa'),
(908, 'Copasa'),
(435, 'São Lucas');

SELECT * FROM Convenio;

INSERT INTO Consulta VALUES (1, '2013-05-10', '10:00', 18739, 1, 189, 5),
(2, '2013-05-12', '10:00', 7646, 2, 232, 10),
(3, '2013-05-12', '11:00', 18739, 3, 908, 15),
(4, '2013-05-13', '10:00', 7646, 4, 435, 13),
(5, '2013-05-14', '13:00', 7646, 2, 232, 10),
(6, '2013-05-14', '14:00', 39872, 1, 189, 5);

SELECT * FROM Consulta;

INSERT INTO Atende VALUES (18739, 189),
(18739, 908),
(7646, 232),
(39872, 189);

SELECT * FROM Atende;

INSERT INTO Possui VALUES (1, 189, 'E', '2016-12-31'),
(2, 232, 'S', '2014-12-31'),
(3, 908, 'S', '2017-12-31'),
(4, 435, 'E', '2016-12-31'),
(1, 232, 'S', '2015-12-31');

SELECT * FROM Possui;

-- 1. Atualize o endereço do paciente João para ‘Rua do Bonde’;
UPDATE Paciente SET endereco = 'Rua do bonde' 
WHERE nome = 'João'

--2. Atualize os dados do medico Elias para ‘Rua Z’ e telefone ‘9838-7867’;
UPDATE Medico SET endereco = 'Rua Z', telefone = '9838-7867' 
WHERE nome = 'Elias'

SELECT * FROM Medico

--3. Atualize todos os tipos dos convênios que os pacientes possuem para ‘S’;
UPDATE Possui SET tipo = 'S'
SELECT * FROM Possui;

--4. Exclua a informação que o paciente José tem o convenio 232;
DELETE FROM Possui 
WHERE Convenio = 232 AND 
Paciente = (SELECT codpac FROM Paciente WHERE nome = 'José');
SELECT * FROM Possui;

--5. Exclua a consulta realizada do dia 14/05/2013 as 14:00.
DELETE FROM Consulta
WHERE data = '2013-05-14' AND horario = '14:00'
SELECT * FROM Consulta;

--6. Altere o nome da coluna especialidade, da tabela médico, 
--para especialização.
ALTER TABLE Medico RENAME COLUMN Especialidade TO Especialazacao 
SELECT * FROM Medico;

--6. Altere o nome da coluna especialidade, da tabela médico, 
--para especialização.
ALTER TABLE Convenio ALTER COLUMN nome TYPE VARCHAR(200)
SELECT * FROM Convenio;

--8. Acrescente a coluna Valor na tabela consulta e atualize todas as 
--consultas para o valor de R$100,00.
ALTER TABLE Consulta ADD Valor VARCHAR(10) DEFAULT '100'
SELECT * FROM Consulta;
